# Trabajo Práctico - Inteligencia Artificial

Trabajo Práctico para la cátedra Inteligencia Artificial de la Universidad Tecnológica Nacional - Facultad Regional Córdoba.

El objetivo de este proyecto es el de aplicar de una manera práctica los conceptos de Reconocimiento de Imágenes desarrollados en clase.

El programa consta de dos partes:
- Detector de diferencias entre dos imágenes
- Detector de rostros

# Integrantes
- Benito, Federico
- Cáceres, Maximiliano
- Populin, Stefano