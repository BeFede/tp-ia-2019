import cv2
import sys
import numpy as np
import os

#Cargamos las dos imagenes para hacer las diferencias
if (len( sys.argv) <= 3):
    sys.exit()


diff1 = cv2.imread(sys.argv[1])
diff2 = cv2.imread(sys.argv[2])
diff2original = cv2.imread(sys.argv[2])
nombre = sys.argv[3]
#Calculamos la diferencia absoluta de las dos imagenes
diff_total = cv2.absdiff(diff1, diff2)

#Buscamos los contornos
imagen_gris = cv2.cvtColor(diff_total, cv2.COLOR_BGR2GRAY)
image, contours,_  = cv2.findContours(image=imagen_gris,mode=cv2.RETR_EXTERNAL,method=cv2.CHAIN_APPROX_SIMPLE)

#Miramos cada uno de los contornos y, si no es ruido, dibujamos su Bounding Box sobre la imagen original
for c in contours:
    if cv2.contourArea(c) >= 20:
        posicion_x,posicion_y,ancho,alto = cv2.boundingRect(c) #Guardamos las dimensiones de la Bounding Box
        cv2.rectangle(diff2,(posicion_x,posicion_y),(posicion_x+ancho,posicion_y+alto),(0,0,255),2) #Dibujamos la bounding box sobre diff1

img1 = np.hstack((diff1, diff2original))
img2 = np.hstack((diff_total, diff2))
imagen_posta = diff2; #np.vstack((img1, img2))

    #while(1):
    #Mostramos las imagenes. ESC para salir.

ruta = os.path.dirname(__file__) + '/imagenes/proceso/' + nombre

cv2.imwrite(ruta, imagen_posta)
    #cv2.imshow('Imagen2', diff2original)
    #cv2.imshow('Imagen con diferencias', diff2)
    #cv2.imshow('Diferencias detectadas', diff_total)
    #tecla = cv2.waitKey(5) & 0xFF
    #if tecla == 27:
    #    break
 

