from django.shortcuts import render
from django.template.loader import get_template
import json
from django.http import HttpResponse
from django.views import View
from rest_framework import viewsets
from django.conf import settings
import subprocess
import os
import time

from django.core.serializers.json import DjangoJSONEncoder

__all__ = ('Storage', 'FileSystemStorage', 'DefaultStorage', 'default_storage')



class IndexView(View):
    def get(self, request, *args, **kwargs):    
        t = get_template('index.html')
        html = t.render({})
        return HttpResponse(html)


class DetectarDiferenciasView(View):

    def get(self, request, *args, **kwargs):        
        t = get_template('deteccion_diferencias_template.html')
        html = t.render({})
        return HttpResponse(html)

    def post(self, request, *args, **kwargs):


        filepath1 = subir_archivo(request.FILES['img1'], "img1")
        filepath2 = subir_archivo(request.FILES['img2'], "img2")

        imagen = self.detectar_diferencias(filepath1, filepath2)
        

        dic = {
            'ruta': imagen
        }
        status_code = 200        
        return HttpResponse(json.dumps(dic, cls=DjangoJSONEncoder), content_type='application/json',
                            status=status_code)


                


    def detectar_diferencias(self, img1, img2):
        
        script =  os.path.dirname(__file__) + '/diferencias.py'        
        
        img_result = str(time.strftime("%H-%M-%S")) + '_diferencias.png' 
        cv2_diferencias = "python2 {0} {1} {2} {3}".format(script, img1, img2, img_result)          
        
        process = subprocess.Popen(cv2_diferencias.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        
        
        return img_result
    


class DetectarRostrosView(View):

    def get(self, request, *args, **kwargs):        
        t = get_template('deteccion_rostros_template.html')
        html = t.render({})
        return HttpResponse(html)

    def post(self, request, *args, **kwargs):

        

        filepath = subir_archivo(request.FILES['img'], "img")        
        imagen = self.detectar_rostros(filepath)
        
        dic = {
            'ruta': imagen
        }
        status_code = 200        
        return HttpResponse(json.dumps(dic, cls=DjangoJSONEncoder), content_type='application/json',
                            status=status_code)

                


    def detectar_rostros(self, img):

        script = os.path.dirname(__file__) + '/detector_rostros.py'
        name = str(time.strftime("%H-%M-%S")) + '_deteccion_rostros.png' 
        img_result = os.path.dirname(__file__) + '/imagenes/proceso/deteccion_rostro.png'
        
        
        cv2_rostros = "python2 {0} {1} {2}".format(script, img, name)  

        process = subprocess.Popen(cv2_rostros.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()


        return name


def subir_archivo(img, name):
    ruta_imagen = os.path.dirname(__file__) + '/temp/' + str(name)
    
    with open(ruta_imagen, 'wb+') as destination:
        for chunk in img.chunks():
            destination.write(chunk)
    
    return ruta_imagen
        