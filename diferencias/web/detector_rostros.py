import cv2
import sys
import os

imagePath = sys.argv[1]
nombre = sys.argv[2]
cascPath = os.path.dirname(__file__) + "/haarcascade_frontalface_default.xml"


faceCascade = cv2.CascadeClassifier(cascPath)


image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

print("asdas")
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30),
    #flags = cv2.CV_HAAR_SCALE_IMAGE
)

print("Found {0} faces!".format(len(faces)))


for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

ruta = os.path.dirname(__file__) + '/imagenes/proceso/' + nombre
print(ruta)

cv2.imwrite(ruta, image)

