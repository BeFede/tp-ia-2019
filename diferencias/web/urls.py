from django.conf.urls import url
from django.urls import include, path

from django.http import HttpResponse
from django.template import loader
from .views import DetectarDiferenciasView, DetectarRostrosView, IndexView
from django.views.static import serve
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^$', IndexView.as_view()),
    url(r'^detectar_diferencias/?$', DetectarDiferenciasView.as_view()),
    url(r'^detectar_rostros/?$', DetectarRostrosView.as_view()),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT, }, name="media"),
]

urlpatterns += staticfiles_urlpatterns()